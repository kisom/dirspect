package dirspect

import "testing"

func TestHumanReadable(t *testing.T) {
	expected := "2.00 GB"
	have := ToHumanSize(2147483648)
	if have != expected {
		t.Fatalf("expected %s, have %s", expected, have)
	}
}
