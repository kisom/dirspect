package dirspect

import "fmt"

const (
	Kilobyte = 1024
	Megabyte = Kilobyte * 1024
	Gigabyte = Megabyte * 1024
	Terabyte = Gigabyte * 1024
)

func ToHumanSize(size int64) string {
	if size > Terabyte {
		return fmt.Sprintf("%0.2f TB", float64(size)/float64(Terabyte))
	} else if size > Gigabyte {
		return fmt.Sprintf("%0.2f GB", float64(size)/float64(Gigabyte))
	} else if size > Megabyte {
		return fmt.Sprintf("%0.2f MB", float64(size)/float64(Megabyte))
	} else if size > Kilobyte {
		return fmt.Sprintf("%0.2f KB", float64(size)/float64(Kilobyte))
	}

	return fmt.Sprintf("%d B", size)
}
