package dirspect

import (
	"os"
	"path/filepath"
	"sort"
)

type RecentlyActiveFiles []Info

func (raf RecentlyActiveFiles) Len() int {
	return len(raf)
}

func (raf RecentlyActiveFiles) Less(i, j int) bool {
	return raf[i].LastActive().After(raf[j].LastActive())
}

func (raf RecentlyActiveFiles) Swap(i, j int) {
	raf[i], raf[j] = raf[j], raf[i]
}

func generateWalker(infos *[]Info) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if !info.Mode().IsRegular() {
			return nil
		}

		stat, statErr := Stat(path)
		if statErr != nil {
			return statErr
		}

		*infos = append(*infos, *stat)
		return nil
	}
}

func ListRecentlyActive(path string, maxSize int64) ([]Info, error) {
	infos := []Info{}
	walker := generateWalker(&infos)

	err := filepath.Walk(path, walker)
	if err != nil {
		return nil, err
	}

	raf := RecentlyActiveFiles(infos)
	sort.Sort(raf)

	totalSize := int64(0)
	for i := range raf {
		if totalSize+raf[i].Size > maxSize {
			return raf[:i], nil
		}
		totalSize += raf[i].Size
	}

	return raf, nil
}

type RecentlyModifiedFiles []Info

func (rmf RecentlyModifiedFiles) Len() int {
	return len(rmf)
}

func (rmf RecentlyModifiedFiles) Less(i, j int) bool {
	return rmf[i].MTime.Before(rmf[j].MTime)
}

func (rmf RecentlyModifiedFiles) Swap(i, j int) {
	rmf[i], rmf[j] = rmf[j], rmf[i]
}

func ListRecentlyModified(path string, maxSize int64) ([]Info, error) {
	infos := []Info{}
	walker := generateWalker(&infos)

	err := filepath.Walk(path, walker)
	if err != nil {
		return nil, err
	}

	rmf := RecentlyModifiedFiles(infos)
	sort.Reverse(rmf)

	totalSize := int64(0)
	for i := range rmf {
		if totalSize+rmf[i].Size > maxSize {
			return rmf[:i], nil
		}
		totalSize += rmf[i].Size
	}

	return rmf, nil
}
