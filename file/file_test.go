package dirspect

import "testing"

func TestStat(t *testing.T) {
	const path = "./README.rst"
	info, err := Stat(path)
	if err != nil {
		t.Fatal(err)
	}

	if info.Path != path {
		t.Fatal("stat: invalid path")
	}

	if info.Size == 0 {
		t.Fatal("stat: invalid size")
	}

	if info.ATime.IsZero() {
		t.Fatal("stat: invalid atime")
	}

	if info.MTime.IsZero() {
		t.Fatal("stat: invalid mtime")
	}

	if info.CTime.IsZero() {
		t.Fatal("stat: invalid ctime")
	}

	if info.Mode == 0 {
		t.Fatal("stat: invalid mode")
	}
}
