package dirspect

import (
	"os"
	"time"
)

// Info contains information about a file.
type Info struct {
	Path  string
	Mode  os.FileMode
	Size  int64
	ATime time.Time
	MTime time.Time
	CTime time.Time
}

func (i Info) LastActive() time.Time {
	if i.ATime.After(i.MTime) {
		return i.ATime
	}

	return i.MTime
}
