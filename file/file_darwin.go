package dirspect

import (
	"os"
	"time"

	"golang.org/x/sys/unix"
)

func Stat(path string) (*Info, error) {
	var stat unix.Stat_t
	err := unix.Stat(path, &stat)
	if err != nil {
		return nil, err
	}

	return &Info{
		Path:  path,
		Mode:  os.FileMode(stat.Mode),
		Size:  stat.Size,
		ATime: time.Unix(stat.Atimespec.Sec, stat.Atimespec.Nsec),
		MTime: time.Unix(stat.Mtimespec.Sec, stat.Mtimespec.Nsec),
		CTime: time.Unix(stat.Ctimespec.Sec, stat.Ctimespec.Nsec),
	}, nil
}
