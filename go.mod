module gitlab.com/kisom/dirspect

go 1.12

require (
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/kisom/glitch v0.0.0-20130927012903-078d5859b75a // indirect
	golang.org/x/sys v0.0.0-20190426135247-a129542de9ae
)
