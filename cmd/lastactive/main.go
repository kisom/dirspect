package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"time"

	"gitlab.com/kisom/dirspect"
)

var maxSize int64 = 10 * dirspect.Megabyte

const dateString = "2006-01-02 15:04:05.999999999 MST"

func verifyInfos(path string, infos []dirspect.Info) error {
	totalSize := int64(0)
	lastTime := time.Unix(0, 0)
	count := 0

	for i := range infos {
		count++
		totalSize += infos[i].Size
		lastActive := infos[i].MTime
		if lastTime.Before(lastActive) {
			fmt.Fprintf(os.Stderr, "last: %s\ncurrent: %s\n",
				lastTime.Format(dateString), lastActive.Format(dateString))
			return errors.New("invalid ordering")
		}
		lastTime = lastActive
	}

	if totalSize > maxSize {
		return errors.New("file list exceeds max size")
	}

	fmt.Printf("%d files verified\n", count)
	return nil
}

func main() {
	flag.Int64Var(&maxSize, "m", maxSize, "")
	flag.Parse()

	for _, path := range flag.Args() {
		started := time.Now()
		infos, err := dirspect.ListRecentlyModified(path, maxSize)
		if err != nil {
			fmt.Fprintf(os.Stderr, "[!] %s: %s\n", path, err)
			continue
		}
		scanComplete := time.Now()

		err = verifyInfos(path, infos)
		if err != nil {
			fmt.Fprintf(os.Stderr, "[!] %s: %s\n", path, err)
			continue
		}
		verifyComplete := time.Now()

		fmt.Printf("%s: scan:%s verify:%s total:%s\n", path,
			scanComplete.Sub(started),
			verifyComplete.Sub(scanComplete),
			verifyComplete.Sub(started))
		for i := range infos {

			fmt.Printf("\t%s: %s (%s)\n",
				infos[i].LastActive().Format(dateString),
				infos[i].Path,
				dirspect.ToHumanSize(infos[i].Size))
		}
	}
}
