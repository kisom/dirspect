package main

import (
	"flag"
	"fmt"
	"math/big"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/kisom/dirspect"
)

func generateWalker(files map[string]*dirspect.Info) filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		if !info.Mode().IsRegular() {
			return nil
		}

		stat, statErr := dirspect.Stat(path)
		if statErr != nil {
			return statErr
		}

		files[path] = stat
		return nil
	}
}

func scanDir(path string) error {
	files := map[string]*dirspect.Info{}

	started := time.Now()
	err := filepath.Walk(path, generateWalker(files))
	if err != nil {
		return err
	}

	var totalSize = big.NewInt(0)
	var totalCount = big.NewInt(int64(len(files)))
	var max = big.NewInt(0)
	var min = big.NewInt(107374182400) // guessing at 100G as a reasonable starting size

	for _, stat := range files {
		size := big.NewInt(stat.Size)
		if size.Cmp(max) == 1 {
			max = size
		}

		if stat.Size > 0 && size.Cmp(min) == -1 {
			min = size
		}

		totalSize = totalSize.Add(totalSize, size)
	}

	average := big.NewInt(0)
	average = average.Div(totalSize, totalCount)
	elapsed := time.Since(started)
	fmt.Printf("%s: elapsed=%s\n\tavg=%s total=%s count=%s\n\tmin=%s max=%s\n",
		path, elapsed, average, totalSize, totalCount, min, max)
	return nil
}

func main() {
	flag.Parse()

	for _, path := range flag.Args() {
		var err error
		path, err = filepath.Abs(path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "[!] %s: %s\n", path, err)
			continue
		}

		err = scanDir(path)
		if err != nil {
			fmt.Fprintf(os.Stderr, "[!] %s: %s\n", path, err)
			continue
		}
	}
}
